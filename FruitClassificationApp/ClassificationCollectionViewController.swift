//
//  ClassificationCollectionViewController.swift
//  FruitClassificationApp
//
//

import UIKit
import SAPFiori

private let reuseIdentifier = "Cell"

class ClassificationCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let autosizingColumnFlow = FUIStandardAutoSizingColumnFlowLayout()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        autosizingColumnFlow.minimumInteritemSpacing = CGFloat(16)
        autosizingColumnFlow.minimumLineSpacing = CGFloat(16)
        autosizingColumnFlow.numberOfColumns = 3
        autosizingColumnFlow.isSingleColumnInCompact = true
        collectionView.collectionViewLayout = autosizingColumnFlow
        // Be aware of recommended margins in compact (left 16) and regular (left 48) mode
        collectionView.contentInset = UIEdgeInsets(top: 16, left: 48, bottom: 16, right: 48)
        
        collectionView.register(FUIItemCollectionViewCell.self,
                                forCellWithReuseIdentifier:
            FUIItemCollectionViewCell.reuseIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentClassificationResult.observations.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier:
            FUIItemCollectionViewCell.reuseIdentifier, for: indexPath) as! FUIItemCollectionViewCell
        
        let singleResult = currentClassificationResult.observations[indexPath.row]
        let classImage = UIImage(imageLiteralResourceName: singleResult.className)
        
        itemCell.detailImageView.image = classImage
        itemCell.title.text = singleResult.className
        itemCell.subtitle.text = self.formatPercent(value: singleResult.confidencePercent)
        
        return itemCell
    }

    
    private func formatPercent(value: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 2;
        formatter.locale = Locale(identifier: Locale.current.identifier)
        let result = formatter.string(from: value as NSNumber);
        return result!;
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
