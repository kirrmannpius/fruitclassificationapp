//
//  ClassificationResult.swift
//  FruitClassificationApp
//
//

import Foundation

public struct SingleClassificationResult {
    var className = ""
    var confidencePercent = 0.00
}

public struct CurrentClassificationResult {
    var observations = [SingleClassificationResult]()
}

public var currentClassificationResult = CurrentClassificationResult()
