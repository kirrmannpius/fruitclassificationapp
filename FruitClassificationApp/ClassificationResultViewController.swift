//
//  ClassificationResultViewController.swift
//  FruitClassificationApp
//
//

import UIKit
import SAPFoundation
import SAPFiori
import SAPCommon

class ClassificationResultViewController: UITableViewController {

    var header = [FUIGridRowItem]()
    let columnWidth:[CGFloat] = [105, -1, 150]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Classification Results"
        
        self.tableView.register(FUIGridTableViewHeader.self, forHeaderFooterViewReuseIdentifier: FUIGridTableViewHeader.reuseIdentifier)
        self.tableView.register(FUIGridTableViewCell.self, forCellReuseIdentifier: FUIGridTableViewCell.reuseIdentifier)
        
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedSectionHeaderHeight = 100
        self.tableView.sectionHeaderHeight = UITableView.automaticDimension
        
        self.tableView.separatorStyle = .none
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        setupTableHeader()
    }

    func setupTableHeader() {
        header = [
            FUIGridRowHeaderItem(text: ""), // no title for Image column
            FUIGridRowHeaderItem(text: "Class")
        ]
        
        let confidenceHeader = FUIGridRowHeaderItem(text: "Confidence")
        confidenceHeader.textAlignment = .right
        header.append(confidenceHeader)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentClassificationResult.observations.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let singleResult = currentClassificationResult.observations[indexPath.row]
        
        let gridCell = tableView.dequeueReusableCell(withIdentifier: FUIGridTableViewCell.reuseIdentifier, for: indexPath) as! FUIGridTableViewCell
        
        
        let classImage = UIImage(imageLiteralResourceName: singleResult.className)
        
        var items: [FUIGridRowItem] = [
            FUIGridRowImageItem(image: classImage),
            FUIGridRowTextItem(text: singleResult.className)
        ]
        
        let confidenceItem = FUIGridRowTextItem(text: self.formatPercent(value: singleResult.confidencePercent))
        confidenceItem.textAlignment = .right
        items.append(confidenceItem)
        
        gridCell.items = items
        
        // -1 means the cell width is dynamically calculated
        gridCell.columnWidth = columnWidth
        gridCell.accessoryType = .none
        gridCell.borders = [.bottom]
        
        return gridCell
    }
    
    private func formatPercent(value: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 2;
        formatter.locale = Locale(identifier: Locale.current.identifier)
        let result = formatter.string(from: value as NSNumber);
        return result!;
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: FUIGridTableViewHeader.reuseIdentifier) as! FUIGridTableViewHeader
        
        headerView.items = header
        
        headerView.columnWidth = columnWidth
        headerView.borders = [.bottom]
        
        return headerView
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
