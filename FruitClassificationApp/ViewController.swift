//
//  ViewController.swift
//  FruitClassificationApp
//
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            guard let convertedCIImage = CIImage(image: originalImage) else {
                fatalError("Cannot convert to CIImage")
            }
            classifyImage(image: convertedCIImage)
            imageView.image = originalImage
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func classifyImage(image: CIImage){
        guard let model = try? VNCoreMLModel(for: FruitImageClassifier().model) else {
            fatalError("Cannot import CoreML model")
        }
        
        let request = VNCoreMLRequest(model: model) { (request, error) in
            guard let classificationResults = request.results as? [VNClassificationObservation] else {
                fatalError("Did not get classification request results")
            }
            guard let bestResult = classificationResults.first else {
                fatalError("Cannot get classification result")
            }
            currentClassificationResult = CurrentClassificationResult.init(observations: classificationResults.filter({ $0.confidence > 0.01 })
                .map(self.observationResult))
            
            print(currentClassificationResult)
            self.navigationItem.title = bestResult.identifier
        }
            
        let handler = VNImageRequestHandler(ciImage: image)
        do {
            try handler.perform([request])
        } catch {
            print(error)
        }
    }
    
    func observationResult(observation: VNClassificationObservation) -> SingleClassificationResult {
        let confidencePercent = observation.confidence
        return SingleClassificationResult.init(className: observation.identifier, confidencePercent: Double(confidencePercent))
    }
    
    @IBAction func onCameraButtonTapped(_ sender: UIBarButtonItem) {
        present(imagePicker, animated: true, completion: nil)
    }
    
}

